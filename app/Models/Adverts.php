<?php

namespace App\Models;


class Adverts extends \Eloquent{
    const TABLE = 'adverts';

    protected $table = self::TABLE;
    protected $guarded = ['id'];

}
