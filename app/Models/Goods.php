<?php

namespace App\Models;


class Goods extends \Eloquent{
    const TABLE = 'goods';

    protected $table = self::TABLE;
    protected $guarded = ['id'];
    public    $timestamps = false;


    public static function get_goods(){
        return Goods::
        join('adverts',function($join){
            $join->on('adverts.user_id', '=','goods.good_advert' );
        })->get();
    }

    public static function save_goods($inputs){
        foreach($inputs['hide_id'] as $key => $id){
            Goods::where('good_id', $id)->update([
                'good_id' => $inputs['id'][$key],
                'good_name'=> $inputs['name'][$key],
                'good_price'=> $inputs['price'][$key],
                'good_advert'=> $inputs['advert'][$key],
            ]);
        }
    }

}
