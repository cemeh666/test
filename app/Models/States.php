<?php

namespace App\Models;


class States extends \Eloquent{
    const TABLE = 'states';

    protected $table = self::TABLE;
    protected $guarded = ['id'];

}
