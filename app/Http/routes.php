<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    if(Auth::guest())
        return view('auth.login');
    return redirect('/orders');
});
Route::get('/orders',       'OrdersController@index');
Route::get('/goods',        'GoodsController@index');
Route::post('/goods',       'GoodsController@edit');
Route::post('/goods/edit',  'GoodsController@save');
Route::get('auth/logout', 'Auth\AuthController@logout');

Route::controllers([
    'auth' => 'Auth\AuthController',
    'password' => 'Auth\PasswordController',
]);


Route::group(array('prefix' => '/orders/api'), function () {
    Route::get('/',           'OrdersController@get');

});

Route::group(array('prefix' => '/goods/api'), function () {
    Route::get('/',           'GoodsController@get');

});
