<?php namespace App\Http\Controllers;


use App\Models\Adverts;
use App\Models\Goods;
use Illuminate\Support\Facades\Input;

class GoodsController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Home Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders your application's "dashboard" for users that
	| are authenticated. Of course, you are free to change or remove the
	| controller as you wish. It is just here to get your app started!
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('auth');
	}

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	public function index()
	{

		return view('goods.index')
			;
	}

	public function get(){

		return [
			'status' => 'Success',
			'goods' => Goods::get_goods(),
		];
	}
	public function edit()
	{
		$lists = [];

		$select_goods = Goods::whereIn('good_id', Input::get('select'))->get();
		$adverts = Adverts::all();
		foreach($adverts as $advert){
			$lists[] =[
				$advert['user_id']  => $advert['user_first_name'].' '. $advert['user_last_name'].'; '.$advert['user_login'],
			];
		}
		return view('goods.edit')
			->with('goods', $select_goods)
			->with('lists', $lists)
			;
	}
	public function save()
	{
		$inputs = Input::all();
		if(isset($inputs['hide_id']))
			Goods::save_goods($inputs);

		return redirect('/goods');

	}


}
