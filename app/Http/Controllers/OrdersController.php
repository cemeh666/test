<?php namespace App\Http\Controllers;


use App\Models\Orders;
use App\Models\States;

class OrdersController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Home Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders your application's "dashboard" for users that
	| are authenticated. Of course, you are free to change or remove the
	| controller as you wish. It is just here to get your app started!
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('auth');
	}

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	public function index()
	{

		return view('orders.index')
//			->with('videos', $videos)
			;
	}

	public function get(){
		$result = Orders::
			join('states',function($join){
				$join->on('orders.order_state', '=','states.state_id' );
			})
			->join('goods',function($join){
				$join->on('orders.order_good', '=','goods.good_id');
			})
//			->join('users',function($join){
//				$join->on('goods.good_advert', '=','users.id');
//			})
			->get();
		return [
			'status' => 'Success',
			'orders' => $result,
			'states' => States::all()->lists('state_name', 'state_slug')
		];
	}


}
