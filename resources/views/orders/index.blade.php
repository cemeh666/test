@extends('layouts.app')
@section('content')
    <div class="container-fluid" ng-app="Test">
        <div class="row">
            <div class="col-md-8 col-md-offset-2" ng-controller="OrdersCtrl">
                <div class="col-md-12">
                    <div class="col-md-3">
                        <div class="form-group">
                            <label>ОТ</label>
                            <input type="text" class="form-control">
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label>ДО</label>
                            <input type="text" class="form-control">
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="form-group">
                            <label>Статус</label>
                            <select class="form-control uk-width-2-3" ng-model="filter.state_slug"
                                    ng-options="key as value for (key, value) in states">
                                <option value="" disabled>Не выбрано</option>
                            </select>
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="form-group">
                            <button ng-click="go_filter()">Показать</button>
                        </div>
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="col-md-3">
                        <div class="form-group">
                            <label>Телефон</label>
                            <input type="text" ng-model="filter.order_client_phone" class="form-control">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Товар</label>
                            <input type="text" ng-model="filter.good_name" class="form-control">
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="form-group">
                            <label>ID заказа</label>
                            <input type="text" ng-model="filter.order_id" class="form-control">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-8">
                            Показывать по <input ng-model="paginate" type="number"> на странице
                        </div>
                        <div class="col-md-4">
                            Поиск <input type="text">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th>#
                                        <span ng-click="sortable('order_id')" class="glyphicon glyphicon-sort-by-order"
                                              aria-hidden="true"></span>
                                        <span ng-click="sortable('order_id', '-')"
                                              class="glyphicon glyphicon-sort-by-order-alt" aria-hidden="true"></span>
                                    </th>
                                    <th>Дата
                                        <span ng-click="sortable('order_add_time')"
                                              class="glyphicon glyphicon-sort-by-alphabet" aria-hidden="true"></span>
                                        <span ng-click="sortable('order_add_time', '-')"
                                              class="glyphicon glyphicon-sort-by-alphabet-alt"
                                              aria-hidden="true"></span>
                                    </th>
                                    <th>Клиент
                                        <span ng-click="sortable('order_client_name')"
                                              class="glyphicon glyphicon-sort-by-alphabet" aria-hidden="true"></span>
                                        <span ng-click="sortable('order_client_name', '-')"
                                              class="glyphicon glyphicon-sort-by-alphabet-alt"
                                              aria-hidden="true"></span>
                                    </th>
                                    <th>Телефон
                                        <span ng-click="sortable('order_client_phone')"
                                              class="glyphicon glyphicon-sort-by-order" aria-hidden="true"></span>
                                        <span ng-click="sortable('order_client_phone', '-')"
                                              class="glyphicon glyphicon-sort-by-order-alt" aria-hidden="true"></span>
                                    </th>
                                    <th>Товар
                                        <span ng-click="sortable('good_name')"
                                              class="glyphicon glyphicon-sort-by-alphabet" aria-hidden="true"></span>
                                        <span ng-click="sortable('good_name', '-')"
                                              class="glyphicon glyphicon-sort-by-alphabet-alt"
                                              aria-hidden="true"></span>
                                    </th>
                                    <th>Статус
                                        <span ng-click="sortable('state_name')"
                                              class="glyphicon glyphicon-sort-by-alphabet" aria-hidden="true"></span>
                                        <span ng-click="sortable('state_name', '-')"
                                              class="glyphicon glyphicon-sort-by-alphabet-alt"
                                              aria-hidden="true"></span>
                                    </th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr dir-paginate="order in orders | filter:filtered | orderBy:sort | itemsPerPage: paginate">
                                    <th>[[order.order_id]]</th>
                                    <td>[[order.order_add_time]]</td>
                                    <td>[[order.order_client_name]]</td>
                                    <td>[[order.order_client_phone]]</td>
                                    <td>[[order.good_name]]</td>
                                    <td>[[order.state_name]]</td>
                                </tr>

                                </tbody>
                            </table>
                            <dir-pagination-controls></dir-pagination-controls>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
