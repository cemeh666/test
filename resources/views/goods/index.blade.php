@extends('layouts.app')
@section('content')
    <div class="container-fluid" ng-app="Test">
        <div class="row">
            <div class="col-md-8 col-md-offset-2" ng-controller="GoodsCtrl">

                <div class="row">
                    <div class="col-md-8">
                        Показывать по <input ng-model="paginate" type="number"> на странице

                    </div>
                    <div class="col-md-4">
                        Поиск <input ng-model="search" type="text">
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <form role="form" method="POST" action="/goods">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">

                            <table class="table" id="table">
                                <thead>
                                <tr>
                                    <th>
                                        <a href="#select">Все</a>
                                        <span class="glyphicon glyphicon-menu-up" aria-hidden="true"></span>
                                    </th>
                                    <th>#
                                        <span ng-click="sortable('good_id')" class="glyphicon glyphicon-sort-by-order"
                                              aria-hidden="true"></span>
                                        <span ng-click="sortable('good_id', '-')"
                                              class="glyphicon glyphicon-sort-by-order-alt" aria-hidden="true"></span>
                                    </th>
                                    <th>
                                        <span ng-click="sortable('good_name')"
                                              class="glyphicon glyphicon-sort-by-alphabet" aria-hidden="true"></span>
                                        <span ng-click="sortable('good_name', '-')"
                                              class="glyphicon glyphicon-sort-by-alphabet-alt"
                                              aria-hidden="true"></span>
                                        Название
                                    </th>
                                    <th>
                                        <span ng-click="sortable('good_price')"
                                              class="glyphicon glyphicon-sort-by-order" aria-hidden="true"></span>
                                        <span ng-click="sortable('good_price', '-')"
                                              class="glyphicon glyphicon-sort-by-order-alt" aria-hidden="true"></span>
                                        Цена
                                    </th>
                                    <th>
                                        <span ng-click="sortable('user_first_name')"
                                              class="glyphicon glyphicon-sort-by-alphabet" aria-hidden="true"></span>
                                        <span ng-click="sortable('user_first_name', '-')"
                                              class="glyphicon glyphicon-sort-by-alphabet-alt"
                                              aria-hidden="true"></span>
                                        Рекламодатель
                                    </th>
                                </tr>
                                </thead>
                                <tbody>

                                <tr dir-paginate="good in goods | filter:{good_name:search}| orderBy:sort  | itemsPerPage: paginate">
                                    <th><input type="checkbox" name="select[]" value="[[good.good_id]]"> [[good.select]]
                                    </th>
                                    <td>[[good.good_id]]</td>
                                    <td>[[good.good_name]]</td>
                                    <td>[[good.good_price]]</td>
                                    <td>[[good.user_first_name]] [[good.user_last_name]]</td>
                                </tr>

                                </tbody>
                            </table>
                            <button type="submit">Редактировать</button>
                        </form>
                        <dir-pagination-controls></dir-pagination-controls>

                    </div>
                </div>

            </div>
        </div>
    </div>

@endsection
