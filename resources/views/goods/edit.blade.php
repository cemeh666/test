@extends('layouts.app')
@section('content')
    <div class="container-fluid" ng-app="Test">
        <div class="row">
            <div class="col-md-8 col-md-offset-2" ng-controller="GoodsCtrl">

                <div class="row">
                    <div class="col-md-8">
                        Основная информация
                    </div>

                </div>
                <div class="row">
                    <form role="form" method="POST" action="/goods/edit">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">

                        @foreach($goods as $good)
                            <input type="hidden" name="hide_id[]" value="{{ $good['good_id'] }}">

                            <div class="col-md-12">
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label class="control-label">ID</label>
                                        <input type="text" class="form-control" name="id[]"
                                               value="{{ $good['good_id']}}">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <label class="control-label">Рекламодатель</label>
                                    {{Form::select('advert[]', $lists, $good['good_advert'], ['class'=>"form-control"])}}
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label">Назване</label>
                                        <input type="text" class="form-control" name="name[]"
                                               value="{{ $good['good_name']}}">
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label class="control-label">Цена</label>
                                        <input type="text" class="form-control" name="price[]"
                                               value="{{$good['good_price'] }}">
                                    </div>
                                </div>

                            </div>
                        @endforeach
                        <button type="submit">Сохранить</button>
                    </form>

                </div>

            </div>
        </div>
    </div>

@endsection
