
var App = {};
// Declare app level module which depends on filters, and services
    angular.module('Test', [
            'ngResource',
            'ngRoute',
            'angularUtils.directives.dirPagination',
            'Test.controllers',
            'Test.services',
            'checklist-model'
        ])
    .config(function($interpolateProvider, $routeProvider, $locationProvider){
    $interpolateProvider.startSymbol('[[').endSymbol(']]');
    $locationProvider.html5Mode({
        enabled: true,
        requireBase: false,
        rewriteLinks: false
    });


});
