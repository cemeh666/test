'use strict';

/* Controllers */
/**
 * Пример описания контроллера
 * .controller('имя контроллера', Имя функции)
 */
angular.module('Test.controllers', [])

    .controller('OrdersCtrl', OrdersCtrl)
    .controller('GoodsCtrl', GoodsCtrl)
;

function OrdersCtrl($scope, OrdersService) {

    $scope.paginate = 3;

    $scope.filter = {
        state_slug: '',
        order_client_phone:'',
        good_name:'',
        order_id:''
    };
    OrdersService.getOrders($scope);

    $scope.go_filter = function(){
        $scope.filtered = angular.copy($scope.filter);
    };
    $scope.sortable = function(item, revers){
        if(revers)
            $scope.sort = revers+item;
        else
            $scope.sort = item;

    };
}

function GoodsCtrl($scope, GoodsService) {
    $scope.paginate = 3;

    GoodsService.getGoods($scope);

    $scope.sortable = function(item, revers){
        if(revers)
            $scope.sort = revers+item;
        else
            $scope.sort = item;
    };

}