'use strict';
//
///* Services */
//
//// Demonstrate how to register services
//// In this case it is a simple value service.
angular.module('Test.services', [])


    .service('OrdersService', function ($rootScope, $http, $resource) {
        var api = this,
            OrdersApi = $resource('/orders/api', {id: '@id'}, {
                getOrders: {method: "GET", url: '/orders/api'}
            });

        api.dataResponse = function (scope, response) {
            if (response.status == 'Success') {
                angular.forEach(response.orders, function (value) {
                    value.order_client_phone = parseInt(value.order_client_phone);
                });
                scope.orders = response.orders;
                scope.states = response.states;
            } else
                alert('Сервер временно не доступен ' + error.status + ' (' + error.statusText + ')');
        };


        api.getOrders = function (scope) {
            OrdersApi.getOrders(function (response) {
                api.dataResponse(scope, response);
            }, function (error) {
                alert('Сервер временно не доступен ' + error.status + ' (' + error.statusText + ')');
            }).$promise.then(function (response) {

            });
        };

    })

    .service('GoodsService', function ($rootScope, $http, $resource) {
        var api = this,
            OrdersApi = $resource('/good/api', {id: '@id'}, {
                getGoods: {method: "GET", url: '/goods/api'}
            });

        api.dataResponse = function (scope, response) {
            if (response.status == 'Success') {
                angular.forEach(response.goods, function (value) {
                    value.good_price = parseInt(value.good_price);
                });
                scope.goods = response.goods;
            } else
                alert('Сервер временно не доступен ' + error.status + ' (' + error.statusText + ')');
        };


        api.getGoods = function (scope) {
            OrdersApi.getGoods(function (response) {
                api.dataResponse(scope, response);
            }, function (error) {
                alert('Сервер временно не доступен ' + error.status + ' (' + error.statusText + ')');
            }).$promise.then(function (response) {

            });
        };

    });
