<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Advert extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(\App\Models\Adverts::TABLE, function (Blueprint $table) {
            $table->increments('user_id');
            $table->char('user_first_name');
            $table->char('user_last_name');
            $table->string('user_login')->unique();
            $table->string('user_password');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(App\Models\Adverts::TABLE);
    }
}
