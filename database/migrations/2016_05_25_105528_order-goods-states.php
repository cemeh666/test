<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class OrderGoodsStates extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create(App\Models\Orders::TABLE, function(Blueprint $t)
        {
            $t->increments('order_id');
            $t->integer('order_state');
            $t->integer('order_good');
            $t->timestamp('order_add_time');
            $t->char('order_client_phone');
            $t->char('order_client_name');
        });

        Schema::create(App\Models\States::TABLE, function(Blueprint $t)
        {
            $t->increments('state_id');
            $t->char('state_name');
            $t->char('state_slug');
        });

        Schema::create(App\Models\Goods::TABLE, function(Blueprint $t)
        {
            $t->increments('good_id');
            $t->char('good_name');
            $t->integer('good_price');
            $t->integer('good_advert');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(App\Models\Orders::TABLE);
        Schema::drop(App\Models\States::TABLE);
        Schema::drop(App\Models\Goods::TABLE);

    }
}
